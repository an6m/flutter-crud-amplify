/*
* Copyright 2021 Amazon.com, Inc. or its affiliates. All Rights Reserved.
*
* Licensed under the Apache License, Version 2.0 (the "License").
* You may not use this file except in compliance with the License.
* A copy of the License is located at
*
*  http://aws.amazon.com/apache2.0
*
* or in the "license" file accompanying this file. This file is distributed
* on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied. See the License for the specific language governing
* permissions and limitations under the License.
*/

// ignore_for_file: public_member_api_docs

import 'package:amplify_datastore_plugin_interface/amplify_datastore_plugin_interface.dart';
import 'package:flutter/foundation.dart';

/** This is an auto generated class representing the Case type in your schema. */
@immutable
class Case extends Model {
  static const classType = const _CaseModelType();
  final String id;
  final String name;
  final String gender;
  final int age;
  final String address;
  final String city;
  final String country;
  final String status;
  final String updated;

  @override
  getInstanceType() => classType;

  @override
  String getId() {
    return id;
  }

  const Case._internal(
      {@required this.id,
      this.name,
      this.gender,
      this.age,
      this.address,
      this.city,
      this.country,
      this.status,
      this.updated});

  factory Case(
      {String id,
      String name,
      String gender,
      int age,
      String address,
      String city,
      String country,
      String status,
      String updated}) {
    return Case._internal(
        id: id == null ? UUID.getUUID() : id,
        name: name,
        gender: gender,
        age: age,
        address: address,
        city: city,
        country: country,
        status: status,
        updated: updated);
  }

  bool equals(Object other) {
    return this == other;
  }

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Case &&
        id == other.id &&
        name == other.name &&
        gender == other.gender &&
        age == other.age &&
        address == other.address &&
        city == other.city &&
        country == other.country &&
        status == other.status &&
        updated == other.updated;
  }

  @override
  int get hashCode => toString().hashCode;

  @override
  String toString() {
    var buffer = new StringBuffer();

    buffer.write("Case {");
    buffer.write("id=" + "$id" + ", ");
    buffer.write("name=" + "$name" + ", ");
    buffer.write("gender=" + "$gender" + ", ");
    buffer.write("age=" + (age != null ? age.toString() : "null") + ", ");
    buffer.write("address=" + "$address" + ", ");
    buffer.write("city=" + "$city" + ", ");
    buffer.write("country=" + "$country" + ", ");
    buffer.write("status=" + "$status" + ", ");
    buffer.write("updated=" + "$updated");
    buffer.write("}");

    return buffer.toString();
  }

  Case copyWith(
      {String id,
      String name,
      String gender,
      int age,
      String address,
      String city,
      String country,
      String status,
      String updated}) {
    return Case(
        id: id ?? this.id,
        name: name ?? this.name,
        gender: gender ?? this.gender,
        age: age ?? this.age,
        address: address ?? this.address,
        city: city ?? this.city,
        country: country ?? this.country,
        status: status ?? this.status,
        updated: updated ?? this.updated);
  }

  Case.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        name = json['name'],
        gender = json['gender'],
        age = json['age'],
        address = json['address'],
        city = json['city'],
        country = json['country'],
        status = json['status'],
        updated = json['updated'];

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'gender': gender,
        'age': age,
        'address': address,
        'city': city,
        'country': country,
        'status': status,
        'updated': updated
      };

  static final QueryField ID = QueryField(fieldName: "case.id");
  static final QueryField NAME = QueryField(fieldName: "name");
  static final QueryField GENDER = QueryField(fieldName: "gender");
  static final QueryField AGE = QueryField(fieldName: "age");
  static final QueryField ADDRESS = QueryField(fieldName: "address");
  static final QueryField CITY = QueryField(fieldName: "city");
  static final QueryField COUNTRY = QueryField(fieldName: "country");
  static final QueryField STATUS = QueryField(fieldName: "status");
  static final QueryField UPDATED = QueryField(fieldName: "updated");
  static var schema =
      Model.defineSchema(define: (ModelSchemaDefinition modelSchemaDefinition) {
    modelSchemaDefinition.name = "Case";
    modelSchemaDefinition.pluralName = "Cases";

    modelSchemaDefinition.authRules = [
      AuthRule(authStrategy: AuthStrategy.PUBLIC, operations: [
        ModelOperation.CREATE,
        ModelOperation.UPDATE,
        ModelOperation.DELETE,
        ModelOperation.READ
      ])
    ];

    modelSchemaDefinition.addField(ModelFieldDefinition.id());

    modelSchemaDefinition.addField(ModelFieldDefinition.field(
        key: Case.NAME,
        isRequired: false,
        ofType: ModelFieldType(ModelFieldTypeEnum.string)));

    modelSchemaDefinition.addField(ModelFieldDefinition.field(
        key: Case.GENDER,
        isRequired: false,
        ofType: ModelFieldType(ModelFieldTypeEnum.string)));

    modelSchemaDefinition.addField(ModelFieldDefinition.field(
        key: Case.AGE,
        isRequired: false,
        ofType: ModelFieldType(ModelFieldTypeEnum.int)));

    modelSchemaDefinition.addField(ModelFieldDefinition.field(
        key: Case.ADDRESS,
        isRequired: false,
        ofType: ModelFieldType(ModelFieldTypeEnum.string)));

    modelSchemaDefinition.addField(ModelFieldDefinition.field(
        key: Case.CITY,
        isRequired: false,
        ofType: ModelFieldType(ModelFieldTypeEnum.string)));

    modelSchemaDefinition.addField(ModelFieldDefinition.field(
        key: Case.COUNTRY,
        isRequired: false,
        ofType: ModelFieldType(ModelFieldTypeEnum.string)));

    modelSchemaDefinition.addField(ModelFieldDefinition.field(
        key: Case.STATUS,
        isRequired: false,
        ofType: ModelFieldType(ModelFieldTypeEnum.string)));

    modelSchemaDefinition.addField(ModelFieldDefinition.field(
        key: Case.UPDATED,
        isRequired: false,
        ofType: ModelFieldType(ModelFieldTypeEnum.string)));
  });
}

class _CaseModelType extends ModelType<Case> {
  const _CaseModelType();

  @override
  Case fromJson(Map<String, dynamic> jsonData) {
    return Case.fromJson(jsonData);
  }
}
